# manushov-rodion.ru
```
Личный сайт-блог
```
* Версия: 1.0.1
* Автор: Manushov Rodion (Манушов Родион) manushov_rodion
* Ссылка: https://vk.com/manushov_rodion
* Email: rodion-krox@mail.ru

#updates

### update 1.0.1
* Добавился блог

# Внимание!
* папку db не публиковать!
* папку demo не публиковать!
* файл README не публиковать!
* файл .gitignore не публиковать!

# System
* core_framework_php 1.0.4
* php: 7.1
* postgreSQL: 9.6

# Доступы
* rodion-krox@mail.ru - 7T#raujrCgBH
