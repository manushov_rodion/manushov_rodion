<?php

namespace components\Pages\Models;

use system\component\Model;
use system\utilites\UtiliteImage;


/**
 * ModelPage class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ModelPage extends Model
{
    final public function getDataPage(string $request) : array
    {
        if ($request === 'demo') {
            return [
                'title'                 => 'DEMO PAGES',
                'image_jpg'             => UtiliteImage::resize('demo.jpg', 300, 500),
                'image_jpg_original'    => UtiliteImage::getUrlFile('demo.jpg'),
                'image_png'             => UtiliteImage::resize('404.png', 200, 200),
                'image_png_original'    => UtiliteImage::getUrlFile('404.png'),
                'image_gif'             => UtiliteImage::resize('404.gif', 300, 200),
                'image_gif_original'    => UtiliteImage::getUrlFile('404.gif'),
            ];
        } elseif ($request === 'auth') {
            return [
                'title' => 'DEMO AUTH PAGES'
            ];
        } else {
            return [];
        }
    }
}