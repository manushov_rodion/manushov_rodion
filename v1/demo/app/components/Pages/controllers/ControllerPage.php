<?php

namespace components\Pages\Controllers;

use system\component\Controller;


/**
 * ControllerPage class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerPage extends Controller
{
    public function index()
    {
        if ($this->args->page === 'redirect') {
            $this->redirect('/');
        }

        $data = $this->model->getDataPage($this->args->page);

        if ($data) {
            $this->storage->header = $this->view->loadComponent('Common.Header@index', ['title' => 'pages']);
            $this->storage->footer = $this->view->loadComponent('Common.Footer@index');

            $this->storage->title = $data['title'];
            $this->storage->images = [
                'img_jpg'       => $data['image_jpg'],
                'original_jpg'  => $data['image_jpg_original'],
                'img_png'       => $data['image_png'],
                'original_png'  => $data['image_png_original'],
                'img_gif'       => $data['image_gif'],
                'original_gif'  => $data['image_gif_original'],
            ];

            $this->view->setHttpBody($this->storage);
        } else {
            $this->view->initComponent('Pages.Page404@index');
        }
    }
}