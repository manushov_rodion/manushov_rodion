<?php

namespace components\Pages\Controllers;

use system\component\Controller;


/**
 * ControllerPageСustom class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerPageСustom extends Controller
{
    public function index()
    {
        $this->view->setHttpBody('DEMO PAGE CUSTOM');
    }
}