<?php

namespace components\Pages\Controllers;

use system\component\Controller;
use system\utilites\UtiliteValidations;


/**
 * ControllerPageAuth class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerPageAuth extends Controller
{
    /**
     * Содержит массив ошибок
     *
     * @var array
     */
    private $errors = [];

    /**
     * Метод инициализации
     *
     * @return void
     */
    public function index()
    {
        if ($this->request->method === 'post' && $this->hasValidation()) {
            $this->auth->set('7331ec0a-5de3-49e4-aa93-3b125169b93b');

            if ($this->auth->token) {
                $this->redirect('/');
            } else {
                $this->storage->errors['system'] = $this->language->get('Временно сервис не доступен!');
            }
        }


        $this->storage->header = $this->view->loadComponent('Common.Header@index', ['title' => 'auth']);
        $this->storage->footer = $this->view->loadComponent('Common.Footer@index');

        $this->storage->errors = $this->errors;

        $this->view->setHttpBody($this->storage);
    }

    /**
     * Метод, который проверяет параметры на валидацию
     *
     * @return boolean
     */
    private function hasValidation() : bool
    {
        if(!$this->params->login && !$this->params->password) {
            $this->errors['all'] = $this->language->get('Некоректный логин или пароль!');
        } else {
            if (!UtiliteValidations::isEMail($this->params->login)) {
                $this->errors['login'] = $this->language->get('Некоректный логин!');
            }

            if (!UtiliteValidations::isPassword($this->params->password)) {
                $this->errors['password'] = $this->language->get('Некоректный пароль!');
            }
        }

        return !$this->errors;
    }
}