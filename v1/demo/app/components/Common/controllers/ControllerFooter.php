<?php

namespace components\Common\controllers;

use system\component\Controller;


/**
 * ControllerFooter class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerFooter extends Controller
{
    public function index()
    {
		$this->storage->js = [
			[ 'url' => URL_ASSETS . 'js/main.js', 'version' => '1.0.0' ]
		];

    	$this->view->setHttpBody($this->storage);
    }
}