<?php

/**
 * Этот файл первичной загрузки приложения.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 */

// Активация строгого режима PHP 
declare(strict_types = 1);

// Проверка версии PHP
if (version_compare(phpversion(), '7.1.0', '<') === true) {
	exit('Необходима: PHP >= 7.1');
}

// Установка констант приложения
define('DIR_ROOT', str_replace('\\', '/', __DIR__) . '/');
define('DIR_APP', DIR_ROOT . 'app/');
define('DIR_SYSTEM', DIR_ROOT . 'system/');
define('DIR_STORAGE', DIR_ROOT . 'storage/');

define('DOMAIN_NAME', (function() : string
{
    $http = 'http://';
    if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') || $_SERVER['SERVER_PORT'] === 443) {
		$http = 'https://';
		$_SERVER['HTTPS'] = true;
    } else {
		$_SERVER['HTTPS'] = false;
	}

    return $http . $_SERVER['SERVER_NAME'];
})());

define('URL_ASSETS', DOMAIN_NAME . '/' . basename(__DIR__)  . '/app/assets/' );
define('URL_STORAGE', DOMAIN_NAME . '/' . basename(__DIR__)  . '/storage/' );

// Определение пользователя IP
if (isset($_SERVER['HTTP_CLIENT_IP'])) {
	$_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_CLIENT_IP'];
} elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	$_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
}

// Автоподгрузка обьектов
spl_autoload_register(function(string $object) 
{
    $chunksNameSpace = explode('\\', $object);
    $file = NULL;

	switch (array_shift($chunksNameSpace)) {
		case 'components' :
			$file = DIR_APP . 'components/' . implode('/', $chunksNameSpace) . '.php';
			break;

		case 'plugins' :
			$file = DIR_APP . 'plugins/' . implode('/', $chunksNameSpace) . '.php';
			break;

		case 'system' :
			$file = DIR_SYSTEM . implode('/', $chunksNameSpace) . '.php';
            break;
	}

	if (file_exists($file)) {
		include $file;
	}
}, true, true);

spl_autoload_extensions('.php');

// Запуск приложения
include DIR_SYSTEM . 'app.php';
