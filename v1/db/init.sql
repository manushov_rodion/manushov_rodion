/*
* Init Data Base
* - примечание: необходимо изменить префиксы таблиц
*
* @author Manushov Rodion <rodion-krox@mail.ru>
* @version 1.0.0
*/

---------------------------------------------------------------------------------------------------------- # INCLUDES
CREATE EXTENSION IF NOT EXISTS pgcrypto;

----------------------------------------------------------------------------------------------------- # DELETE TABLES

DROP TABLE IF EXISTS xyz_session_auth;
DROP TABLE IF EXISTS xyz_users;

--------------------------------------------------------------------------------------------------------- # XYZ_USERS
CREATE TABLE xyz_users (
	"id" UUID DEFAULT gen_random_uuid(),
	"login" VARCHAR(64) NOT NULL,
	"password" VARCHAR(128) NOT NULL,
	"name" VARCHAR(64) NOT NULL,
	"sur_name" VARCHAR(64) NOT NULL,
	"middle_name" VARCHAR(64),
	"fio" VARCHAR(70) NOT NULL,
	"email" VARCHAR(64) NOT NULL,
	"date_registration" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	"status" BOOL NOT NULL DEFAULT false 
);

-- inserts
INSERT INTO xyz_users VALUES ('7331ec0a-5de3-49e4-aa93-3b125169b93b', 'login', 'password', 'name', 'sur_name', 'middle_name', 'Admin', 'email@email.ru', DEFAULT, 't');

-- alters
ALTER TABLE xyz_users ADD PRIMARY KEY ("id");


-------------------------------------------------------------------------------------------------- # XYZ_SESSION_AUTH
CREATE TABLE xyz_session_auth (
	"id" UUID DEFAULT gen_random_uuid(),
	"user_id" UUID NOT NULL,
	"token" TEXT NOT NULL,
	"key" VARCHAR(128) NOT NULL,
	"ip" VARCHAR(64) NOT NULL,
    "agent" TEXT NOT NULL,
	"exp" BIGSERIAL NOT NULL,
	"date" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- alters
ALTER TABLE xyz_session_auth ADD FOREIGN KEY ("user_id") REFERENCES xyz_users ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE xyz_session_auth ADD PRIMARY KEY ("id");