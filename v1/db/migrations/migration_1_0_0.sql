/*
* Migration
* - Добавляет блог в бд.
*
* @author Manushov Rodion <rodion-krox@mail.ru>
* @version 1.0.0
*/

DROP TABLE IF EXISTS xyz_blog__posts;

--------------------------------------------------------------------------------------------------- # XYZ_BLOG__POSTS
CREATE TABLE xyz_blog__posts (
	"post_id" UUID DEFAULT gen_random_uuid(),

    "title" VARCHAR(64) NOT NULL,
    "content" TEXT NOT NULL,
    "description" VARCHAR(255) NOT NULL,

    "meta_title" VARCHAR(128) NOT NULL,
    "meta_description" VARCHAR(255) NOT NULL,

    "image" VARCHAR(255) DEFAULT NULL,
    "link" VARCHAR(128) NOT NULL,
    "lang" VARCHAR(2) NOT NULL,

    "date" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "date_lastmod" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "status" BOOL NOT NULL DEFAULT false
);

-- alters
ALTER TABLE xyz_blog__posts ADD PRIMARY KEY ("post_id");


