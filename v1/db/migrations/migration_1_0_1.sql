/*
* Migration
*
* @author Manushov Rodion <rodion-krox@mail.ru>
* @version 1.0.1
*/

ALTER TABLE xyz_users ADD secret_key_password VARCHAR(64) NOT NULL;  
