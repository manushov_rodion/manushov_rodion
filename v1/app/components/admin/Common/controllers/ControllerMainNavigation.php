<?php

namespace components\admin\Common\controllers;

use system\component\Controller;


/**
 * ControllerMainNavigation class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerMainNavigation extends Controller
{
    /**
     * Главный метод инициализации компонента
     *
     * @return void
     */
    public function index()
    {
		$this->storage->nav = [];

        // home
        $this->storage->nav['home'] = [
            'name'      => $this->language->get('Рабочий стол'),
            'link'      => '/xyz',
            'active'    => false,
            'icon'      => 'ti-home',
            'children'  => [],
        ];

        // posts
        $posts = [
            'list' => [
                'name'      => $this->language->get('Список статей'),
                'link'      => '/xyz/posts/list',
                'active'    => false,
                'icon'      => 'ti-book',
                'children'  => [],
            ],
            'add' => [
                'name'      => $this->language->get('Добавить статью'),
                'link'      => '/xyz/posts/add',
                'active'    => false,
                'icon'      => 'ti-marker-alt',
                'children'  => [],
            ],
            'trash' => [
                'name'      => $this->language->get('Корзина'),
                'link'      => '/xyz/posts/trash',
                'active'    => false,
                'icon'      => 'ti-trash',
                'children'  => [],
            ],
        ];

        $this->storage->nav['posts'] = [
            'name'      => $this->language->get('Статьи'),
            'link'      => '',
            'active'    => false,
            'icon'      => 'ti-agenda',
            'children'  => $posts,
        ];

        // set active
        if ($this->args->active) {
            $key = '';
            if (isset($this->args->active['key']) && $this->args->active['key']) {
                $key = $this->args->active['key'];
            }

            $children = '';
            if (isset($this->args->active['children']) && $this->args->active['children']) {
                $children = $this->args->active['children'];
            }

            if (isset($this->storage->nav[$key])) {
                $this->storage->nav[$key]['active'] = true;

                if (isset($this->storage->nav[$key]['children'][$children])) {
                    $this->storage->nav[$key]['children'][$children]['active'] = true;
                }
            }
        }

    	$this->view->setHttpBody($this->storage);
    }
}