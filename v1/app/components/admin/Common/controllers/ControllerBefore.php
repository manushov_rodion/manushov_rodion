<?php

namespace components\admin\Common\controllers;

use system\component\Controller;


/**
 * ControllerBefore class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerBefore extends Controller
{
	/**
     * Главный метод инициализации компонента
     *
     * @return void
     */
    public function index()
    {
		$this->storage->css = [
			[ 'url' => URL_ASSETS . 'css/admin/main.min.css', 'version' => '1.0.0' ]
		];

		$this->storage->title = DOMAIN_NAME;
		if (isset($this->args->title) && $this->args->title) {
			$this->storage->title = $this->args->title;
		}

		$navigation = [];
		if (isset($this->args->active_menu) && $this->args->active_menu) {
			$navigation = [
				'active' => $this->args->active_menu
			];
		}
		$this->storage->main_navigation = $this->view->loadComponent('admin.Common.MainNavigation@index', $navigation);

    	$this->view->setHttpBody($this->storage);
    }
}