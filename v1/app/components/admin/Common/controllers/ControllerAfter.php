<?php

namespace components\admin\Common\controllers;

use system\component\Controller;


/**
 * ControllerAfter class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerAfter extends Controller
{
	/**
     * Главный метод инициализации компонента
     *
     * @return void
     */
    public function index()
    {
		$this->storage->js = [
			[ 'url' => URL_ASSETS . 'js/admin/main.js', 'version' => '1.0.0' ]
		];

    	$this->view->setHttpBody($this->storage);
    }
}