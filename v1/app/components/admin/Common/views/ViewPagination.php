<nav class="pagination">
	<a class="pagination__item pagination__item_first" href="<?php echo $storage->first['link'] ?>"><?php $language->the('Начало') ?></a>

	<?php foreach ($storage->list as $pagination) : ?>

		<?php if ($pagination['type'] === 'link') : ?>
			<?php if ($pagination['active'] === true) : ?>
				<span class="pagination__item pagination__item_active"><?php echo $pagination['name'] ?></span>
			<?php else: ?>
				<a class="pagination__item" href="<?php echo $pagination['link'] ?>"><?php echo $pagination['name'] ?></a>
			<?php endif ?>
		<?php else : ?>
			<span class="pagination__item pagination__item_list"><?php echo $pagination['name'] ?></span>
		<?php endif ?>
	<?php endforeach ?>

	<a class="pagination__item pagination__item_last" href="<?php echo $storage->last['link'] ?>"><?php $language->the('Конец') ?></a>
</nav>