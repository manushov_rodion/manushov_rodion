<!DOCTYPE html>
<html lang="<?php echo $language->lang ?>">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">

		<link rel="apple-touch-icon" sizes="180x180" href="/v1/app/assets/img/favicons/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/v1/app/assets/img/favicons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/v1/app/assets/img/favicons/favicon-16x16.png">
		<link rel="manifest" href="/v1/app/assets/img/favicons/site.webmanifest">
		<meta name="msapplication-TileColor" content="#242d37">
		<meta name="theme-color" content="#242d37">

	<?php if (isset($storage->css)) : foreach ($storage->css as $css) : ?>
		<link rel="stylesheet" href="<?php echo $css['url'] ?>?version=<?php echo $css['version'] ?>">
	<?php endforeach; endif ?>

		<title><?php echo $storage->title ?></title>
	</head>
	<body>
		<div class="wrapper">
			<div class="wrapper__left">
				<h1 class="logo">ADMINKA</h1>
				<a class="exit" href="/xyz/auth/exit"><i class="ti-new-window"></i> <?php $language->the('Выход') ?></a>
				<hr>

				<?php echo $storage->main_navigation ?>

			</div>
			<div class="wrapper__right">
		