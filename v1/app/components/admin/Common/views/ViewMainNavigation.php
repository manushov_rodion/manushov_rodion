<?php if ($storage->nav) : ?>
    <nav class="navigation-main">
        <?php foreach ($storage->nav as $nav) : ?>

            <?php if ($nav['children']) : ?>

                <?php
                    if ($nav['active']) {
                        $before = '<div class="navigation-main__item navigation-main__item_active">';
                        $after = '</div>';
                    } else {
                        $before = '<div class="navigation-main__item">';
                        $after = '</div>';
                    }
                ?>

                <?php echo $before ?>
                    <span class="navigation-main__children-name">
                        <i class="<?php echo $nav['icon'] ?>"></i>
                        <?php echo $nav['name'] ?>
                    </span>

                    <nav class="navigation-main__childrens">
                        <?php foreach ($nav['children'] as $children) : ?>

                            <?php
                                if ($children['active']) {
                                    $before = '<span class="navigation-main__item navigation-main__item_active">';
                                    $after = '</span>';
                                } else {
                                    $before = '<a class="navigation-main__item navigation-main__item_hover" href="' . $children['link'] . '">';
                                    $after = '</a>';
                                }
                            ?>

                            <?php echo $before ?>
                                <i class="<?php echo $children['icon'] ?>"></i>
                                <?php echo $children['name'] ?>
                            <?php echo $after ?>

                        <?php endforeach ?>
                    </nav>
                <?php echo $after ?>

            <?php else: ?>

                <?php
                    if ($nav['active']) {
                        $before = '<span class="navigation-main__item navigation-main__item_active">';
                        $after = '</span>';
                    } else {
                        $before = '<a class="navigation-main__item navigation-main__item_hover" href="' . $nav['link'] . '">';
                        $after = '</a>';
                    }
                ?>

                <?php echo $before ?>
                    <i class="<?php echo $nav['icon'] ?>"></i>
                    <?php echo $nav['name'] ?>
                <?php echo $after ?>

            <?php endif ?>

        <?php endforeach ?>
    </nav>
<?php endif ?>