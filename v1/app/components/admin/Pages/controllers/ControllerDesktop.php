<?php

namespace components\admin\Pages\Controllers;

use system\component\Controller;


/**
 * ControllerDesktop class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerDesktop extends Controller
{
    /**
     * Главный метод инициализации компонента
     *
     * @return void
     */
    public function index()
    {
        $this->storage->before = $this->view->loadComponent('admin.Common.Before@index', [
            'title' => $this->language->get('Рабочий стол'),
            'active_menu' => [ 'key' => 'home', 'children' => '' ],
        ]);

        $this->storage->after = $this->view->loadComponent('admin.Common.After@index');

        $this->view->setHttpBody($this->storage);
    }
}