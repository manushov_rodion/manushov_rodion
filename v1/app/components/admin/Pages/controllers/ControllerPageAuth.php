<?php

namespace components\admin\Pages\Controllers;

use system\component\Controller;
use system\utilites\UtiliteValidations;


/**
 * ControllerPageAuth class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerPageAuth extends Controller
{
    /**
     * Содержит массив ошибок
     *
     * @var array
     */
    private $errors = [];
    
    /**
     * Главный метод инициализации компонента
     *
     * @return void
     */
    public function index()
    {
        if ($this->request->method === 'post' && $this->hasValidation()) {
            $userId = $this->model->hasUser($this->params->login, $this->params->password);

            if (!$userId) {
                $this->errors['all'] = $this->language->get('Некоректный логин или пароль!');
            } else {
                $this->auth->set($userId);
            }
            
            if (!$this->auth->token) {
                $this->errors['all'] = $this->language->get('Cервис, временно, не доступен!');
            }
        }

        if ($this->auth->token) {
            $this->redirect('/xyz');
        }

        // Подготовка шаблона
        $breadcrumbs = $this->storage->header = $this->view->loadComponent('site.Common.Breadcrumbs@index', ['breadcrumbs' => [
            [ 'name' => $this->language->get('Авторизация'), 'link' => '' ],
        ] ]);

        $this->storage->header = $this->view->loadComponent('site.Common.Header@index', [
            'title'         => $this->language->get('Авторизация'),
            'breadcrumbs'   => $this->language->get($breadcrumbs),
        ]);

        $this->storage->footer = $this->view->loadComponent('site.Common.Footer@index');

        $this->storage->errors = $this->errors;
        $this->view->setHttpBody($this->storage, true);
    }

    /**
     * Метод, который проверяет параметры на валидацию
     *
     * @return boolean
     */
    private function hasValidation() : bool
    {
        if(!$this->params->login && !$this->params->password) {
            $this->errors['all'] = $this->language->get('Некоректный логин или пароль!');
        } else {
            if (!UtiliteValidations::isEMail($this->params->login)) {
                $this->errors['login'] = $this->language->get('Некоректный логин!');
            }

            if (!UtiliteValidations::isPassword($this->params->password)) {
                $this->errors['password'] = $this->language->get('Некоректный пароль!');
            }
        }

        return !$this->errors;
    }

    /**
     * Метод, который позволяет выйти из админки
     *
     * @return void
     */
    public function exit()
    {
        $this->auth->delete($this->auth->user_id);
        $this->redirect($this->auth->url);
    }
}