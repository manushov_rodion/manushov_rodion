<?php

namespace components\admin\Pages\Models;

use system\component\Model;
use system\utilites\UtiliteRandom;


/**
 * ModelPageAuth class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ModelPageAuth extends Model
{
    /**
     * Метод, который проверяет актуальность данных пользователя, который запросил доступ. Возвращает ID пользователя, если данные корректны.
     *
     * @param string $login
     * @param string $password
     * @return string
     */
    public function hasUser(string $login, string $password) : ?string
    {
        $query = $this->db->query('SELECT id, password, secret_key_password FROM ' . DB_PREFIX . 'users WHERE status = true AND login = ?', [$login]);
        if (!$query->row) {
            return null;
        }

        $password = UtiliteRandom::getHash($password, $query->row->secret_key_password);

        if ($password === $query->row->password) {
            return $query->row->id;
        }

        return null;
    }
}
