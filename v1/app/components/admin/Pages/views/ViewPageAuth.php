<?php echo $storage->header ?>

<div class="section">
    <div class="container">

        <div class="grid">
            <div class="grid__col grid__col_sm-33"></div>
            <div class="grid__col grid__col_sm-33">
                <form method="POST" class="form">

                    <div class="form__group">
                        <input type="email" name="login" placeholder="<?php $language->the('Логин') ?>">
                        <?php if (isset($storage->errors['login']) && $storage->errors['login']) : ?>
                            <p class="color-red"><?php echo $storage->errors['login'] ?></p>
                        <?php endif ?>
                    </div>

                    <div class="form__group">
                        <input type="password" name="password" placeholder="<?php $language->the('Пароль') ?>">
                        <?php if (isset($storage->errors['password']) && $storage->errors['password']) : ?>
                            <p class="color-red"><?php echo $storage->errors['password'] ?></p>
                        <?php endif ?>
                    </div>

                    <input type="submit" class="btn btn_lg" value="<?php $language->the('Авторизоваться') ?>">
                    <?php if (isset($storage->errors['all']) && $storage->errors['all']) : ?>
                        <p class="color-red"><?php echo $storage->errors['all'] ?></p>
                    <?php endif ?>

                </form>
            </div>
        </div>

    </div>
</div>

<?php echo $storage->footer ?>