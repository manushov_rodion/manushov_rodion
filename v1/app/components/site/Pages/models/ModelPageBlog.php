<?php

namespace components\site\Pages\Models;

use system\component\Model;
use system\utilites\UtiliteImage;



/**
 * ModelPageBlog class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ModelPageBlog extends Model
{
    /**
     * Метод, который возвращает список записей
     *
     * @param integer $start
     * @param integer $limit
     * @return array
     */
    public function getPosts(int $start, int $limit) : array
    {
        $data = [];

        $sql = 'SELECT post_id, title, description, image, link, date 
            FROM ' . DB_PREFIX . 'blog__posts 
            WHERE status = true AND lang = ? 
            ORDER BY date DESC LIMIT ? OFFSET ?
        ';

        $query = $this->db->query($sql, ['ru', $limit, $start]);

        if ($query->rows) {
            foreach ($query->rows as $key => $row) {
                $data[$key] = $row;
                $data[$key]->link = '/blog/posts/' . $data[$key]->link;

                if ($data[$key]->image) {
                    $data[$key]->image = UtiliteImage::resize($data[$key]->image, 800, 500);
                }

                $data[$key]->date = $this->dateFormatHTML($data[$key]->date);
            }
        }

        return $data;
    }

    /**
     * Метод, который возвращает общее кол-во записей
     *
     * @return integer
     */
    public function getTotalPosts() : int
    {
        $query = $this->db->query('SELECT COUNT(post_id) AS total FROM ' . DB_PREFIX . 'blog__posts WHERE status = true');
        return (int) $query->row->total;
    }

    /**
     * Фоматирует дату в необходимый формат списка постов.
     *
     * @param string $date
     * @return string
     */
    private function dateFormatHTML(string $date) : string
    {
        $months = [
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',
        ];

        $date = new \DateTime($date);
        [$year, $month, $day] = explode('-', $date->format('Y-m-d'));

        $month = '<span class="posts__date-month">' . $months[$month] . '</span>';
        $day = '<span class="posts__date-day">' . $day . '</span>';
        $year = '<span class="posts__date-year">' . $year . '</span>';

        return  $month . '<strong>' . $day . ' / ' . $year . '</strong>';
    }
}
