<?php

namespace components\site\Pages\Models;

use system\component\Model;
use system\utilites\UtiliteImage;



/**
 * ModelPageBlogPost class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ModelPageBlogPost extends Model
{
    /**
     * Возвращает данные по записи
     *
     * @param string $url
     * @return array
     */
    public function getPost(string $url) : array
    {
        $sql = 'SELECT title, content, image, meta_title, meta_description, date 
            FROM ' . DB_PREFIX . 'blog__posts 
            WHERE status = true AND lang = ? AND link = ?
        ';

        $query = $this->db->query($sql, ['ru', $url]);

        if ($query->row) {
            if ($query->row->image) {
                $query->row->image = UtiliteImage::resize($query->row->image, 800, 500);
            }

            $query->row->date = $this->dateFormatString($query->row->date);
            
            return (array) $query->row;
        }

        return [];
    }

    /**
     * Фоматирует дату в необходимый формат.
     *
     * @param string $date
     * @return string
     */
    private function dateFormatString(string $date) : string
    {
        $months = [
            '01' => 'Январь',
            '02' => 'Февраль',
            '03' => 'Март',
            '04' => 'Апрель',
            '05' => 'Май',
            '06' => 'Июнь',
            '07' => 'Июль',
            '08' => 'Август',
            '09' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',
        ];

        $date = new \DateTime($date);
        [$year, $month, $day] = explode('-', $date->format('Y-m-d'));

        $month = $months[$month];

        return  $month . ' / ' . $day . ' / ' . $year;
    }
}
