<?php echo $storage->header ?>

<div class="section">
    <div class="container">
        <div class="grid">

            <div class="grid__col grid__col_sm-70 posts">
                <?php if ($storage->posts) : ?>
                    <?php foreach ($storage->posts as $index => $post) : ?>

                        <section class="posts__container">
                            <h2 class="posts__title">
                                # <a href="<?php echo $post->link ?>"><?php echo $post->title ?></a>
                            </h2>

                            <?php if ($post->image) : ?>
                                <a href="<?php echo $post->link ?>">
                                    <img class="posts__image" src="<?php echo $post->image ?>" alt="<?php echo $post->title ?>">
                                </a>
                            <?php endif ?>

                            <div class="posts__info">
                                <div class="posts__date"><?php echo $post->date ?></div>
                                <div class="posts__desc"><?php echo $post->description ?></div>
                            </div>

                            <div class="posts__btns">
                                <a class="btn" href="<?php echo $post->link ?>"><?php $language->the('Подробнее') ?></a>
                            </div>
                        </section>

                    <?php endforeach ?>

                    <?php echo $storage->pagination ?>

                <?php else: ?>
                    <p class="posts__no-list"><?php $language->the('Статей еще нет!') ?></p>

                <?php endif ?>
            </div>

            <div class="grid__col grid__col_sm-30">
                <!-- sidebar -->
            </div>
        </div>
    </div>
</div>

<?php echo $storage->footer ?>