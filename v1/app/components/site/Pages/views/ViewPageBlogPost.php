<?php echo $storage->header ?>

<div class="section">
    <div class="container">

        <div class="grid">

            <div class="grid__col grid__col_sm-70 post">

                <?php if ($storage->post['image']) : ?>
                    <img class="post__image" src="<?php echo $storage->post['image'] ?>" alt="<?php echo $storage->post['title'] ?>">
                <?php endif ?>

                <?php echo $storage->post['content'] ?>

                <p class="post__date">
                    <span><?php echo $storage->post['date'] ?></span>
                </p>
            </div>

            <div class="grid__col grid__col_sm-30">
                <!-- sidebar -->
            </div>
        </div>
        
    </div>
</div>

<?php echo $storage->footer ?>