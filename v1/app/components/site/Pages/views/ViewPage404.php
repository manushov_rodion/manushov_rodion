<?php echo $storage->header ?>

<div class="section text-center">
    <img class="page-404__img" src="<?php echo $storage->img ?>">
    <p><?php $language->the('<nobr>Страница не найдена</nobr> | <nobr>Not Found</nobr>') ?></p>
</div>

<?php echo $storage->footer ?>