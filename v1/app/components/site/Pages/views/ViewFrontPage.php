<?php echo $storage->header ?>

<section class="section">
    <div class="container">
        <div class="grid">

            <div class="grid__col grid__col_sm-50">
                <h2 class="front-page__title animated" id="about"><span class="color-blue">#</span> <?php $language->the('Об авторе') ?></h2>

                <p>Меня зовут Манушов Родион Андреевич!</p>
                <p>С 2014 года и по сей день, являюсь как PHP Backend Developer,<br>так и WEB Front-end Developer - проще говоря: Full-stack WEB Developer.</p>
                <p>Интересует меня как разработка пользовательского интерфейса, так и разработка серверной части приложения: всякие самописные core, API, SQL процедуры и т.п.</p>
                <p>Помимо самописок использую различные CMS и Framework-и. Предпочитаю использовать такие инструменты как:  WordPress, OpenCart3, Vue.js и Laravel - на текущий год, 2018, они актуальнык как никогда!</p>
                <p>В последнее время ушёл с головой в разработку сложных систем, построенных на Single Page Application (SPA), в связке с REST API и мощными Базами Данных.</p>
                <p>Помимо рабочих проектов, грешу личными проектами, благодаря которым я развиваюсь очень активно.</p>
                <p>Так получилось, что мое основное хобби - это программирование, сложных приложений, построенных, на WEB технологиях, так что, - стараюсь делать проекты качественно.</p>
                <p>Пытаюсь отдыхать от Компьютера и работы, с помощью как экстремальных мероприятий, так и других возможных увлечений.</p>
            </div>

            <div class="grid__col grid__col_sm-50 front-page__skill-wrapper">

                <div class="front-page__skill m-t_40">
                    <div class="front-page__skill-name">
                        <span style="color: #8892bf">PHP</span>
                    </div>

                    <div class="front-page__skill-info">
                        <span><?php $language->the('Знания на уровне написания реализаций, версии PHP 5.6+: MVC+L, REST API и Framework.') ?></span>
                    </div>
                </div>

                <div class="front-page__skill front-page__skill_right">
                    <div class="front-page__skill-name">
                        <span>CMS</span>
                    </div>

                    <div class="front-page__skill-info">
                        <span><?php $language->the('Удалось поработать с: WordPress, OpenCart, 1C Битрикс, Drupal 7, и Joomla.') ?></span>
                    </div>
                </div>

                <div class="front-page__skill">
                    <div class="front-page__skill-name">
                        <span style="color: #f5da55">JS</span>
                    </div>

                    <div class="front-page__skill-info">
                        <span><?php $language->the('На JavaScript могу писать как c JQuery,<br>так и без него, используя ES6+!') ?></span>
                    </div>
                </div>

                <div class="front-page__skill front-page__skill_right">
                    <div class="front-page__skill-name">
                        <span style="color: #4d80aa">SQL</span>
                    </div>

                    <div class="front-page__skill-info">
                        <span><?php $language->the('Пишу с удовольствие сложные запросы и процедуры, на: PostgeSQl, MSSQL и MySQL.') ?></span>
                    </div>
                </div>

                <div class="front-page__skill">
                    <div class="front-page__skill-name">
                        <span style="color: #4fc08d">VUE.js</span>
                    </div>

                    <div class="front-page__skill-info">
                        <span><?php $language->the('Недавно начал писать на данной библиотеке - есть готовый проект! Еще пишу на Angular 1 и пробывал React.') ?></span>
                    </div>
                </div>

                <div class="front-page__skill front-page__skill_right">
                    <div class="front-page__skill-name">
                        <span style="color: #83d0f2">CSS</span>
                    </div>

                    <div class="front-page__skill-info">
                        <span><?php $language->the('SCSS, LESS, Styles, PostCSS, Отзывчивая верстка - легко!') ?></span>
                    </div>
                </div>

                <div class="front-page__skill">
                    <div class="front-page__skill-name">
                        <span>#</span>
                    </div>

                    <div class="front-page__skill-info">
                        <span><?php $language->the('Git, Jira, Webpack, Gulp, Linux, Bootstrap, Laravel, Slim, Twig, ООП, SPA и еще немного чего.') ?></span>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="section front-page__section-blog">
    <h2># <?php $language->the('Блог автора | разработчика') ?></h2>
    <a href="/blog/posts" class="btn btn_red btn_lg front-page__section-blog_btn"><?php $language->the('Перейти') ?></a>
</section>

<?php echo $storage->footer ?>