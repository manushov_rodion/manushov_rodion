<?php

namespace components\site\Pages\Controllers;

use system\component\Controller;


/**
 * ControllerFrontPage class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerFrontPage extends Controller
{
    /**
     * Главный метод инициализации компонента
     *
     * @return void
     */
    public function index()
    {
        $header = [
            'title'         => $this->language->get('Манушов Родион'),
            'desc'          => $this->language->get('WEB Front-end / PHP Backend Developer'),
            'meta_title'    => $this->language->get('Манушов Родион / manushov_rodion | WEB Full-Stack'),
            'meta_desc'     => $this->language->get('С 2014 года и по сей день, являюсь как PHP Backend Developer, так и WEB Front-end Developer - или, проще говоря: Full-stack WEB Developer.')
        ];

        $this->storage->header = $this->view->loadComponent('site.Common.Header@index', $header);
        $this->storage->footer = $this->view->loadComponent('site.Common.Footer@index');

        $this->view->setHttpBody($this->storage);
    }
}