<?php

namespace components\site\Pages\Controllers;

use system\component\Controller;


/**
 * ControllerPage404 class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerPage404 extends Controller
{
    /**
     * Главный метод инициализации компонента
     *
     * @return void
     */
    public function index()
    {
        $this->storage->header = $this->view->loadComponent('site.Common.Header@index', [
            'title' => $this->language->get('Страница не найдена!'),
            'desc' => $this->language->get('И как Вы сюда зашли?'),
        ]);

        $this->storage->footer = $this->view->loadComponent('site.Common.Footer@index');

        $this->storage->img = URL_ASSETS . 'img/404.png';

        $this->view->setHttpHeader('HTTP/1.1', '404 Not Found');
        $this->view->setHttpBody($this->storage, true);
    }
}