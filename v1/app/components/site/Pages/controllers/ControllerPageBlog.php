<?php

namespace components\site\Pages\Controllers;

use system\component\Controller;
use system\utilites\UtiliteValidations;


/**
 * ControllerFrontPage class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerPageBlog extends Controller
{
    /**
     * Содержит лимит записей на странице
     */
    private const LIMIT_POSTS = 5;

    /**
     * Главный метод инициализации компонента
     *
     * @return void
     */
    public function index()
    {
        $this->storage->posts = [];

        $total = $this->model->getTotalPosts();
        $page = 1;
        if ($total > 0) {
            if ($this->params->page && UtiliteValidations::isIntID($this->params->page)) {
                $page = $this->params->page;

                if ($page > ceil($total / self::LIMIT_POSTS)) {
                    $this->view->initComponent('site.Pages.Page404@index');
                    return;
                }
            }

            $start = ($page - 1) * self::LIMIT_POSTS;

            $this->storage->posts = $this->model->getPosts($start, self::LIMIT_POSTS);
        }

        // Подготовка шаблона
        $breadcrumbs = [
            [ 'name' => $this->language->get('Блог автора | разработчика'), 'link' => '' ]
        ];
        $breadcrumbs = $this->storage->header = $this->view->loadComponent('site.Common.Breadcrumbs@index', ['breadcrumbs' => $breadcrumbs ]);

        $header = [
            'title'         => $this->language->get('Блог автора / разработчика'),
            'breadcrumbs'   => $this->language->get($breadcrumbs),
            'meta_title'    => $this->language->get('Блог автора | разработчика | Манушов Родион'),
            'meta_desc'     => $this->language->get('Раздел, где Я веду свой блог. В основном он связан с моими личными открытиями, проектами или потребностями накидать себе пару заметок, чтобы не забыть.')
        ];
        $this->storage->header = $this->view->loadComponent('site.Common.Header@index', $header);

        $this->storage->footer = $this->view->loadComponent('site.Common.Footer@index');

        $pagination = [
            'page'  => $page,
            'limit' => self::LIMIT_POSTS,
            'total' => $total,
            'link'  => '/blog/posts'
        ];
        $this->storage->pagination = $this->view->loadComponent('site.Common.Pagination@index', $pagination);

        // Вывод
        $this->view->setHttpBody($this->storage, true);
    }
}