<?php

namespace components\site\Pages\Controllers;

use system\component\Controller;
use system\utilites\UtiliteValidations;


/**
 * ControllerFrontPage class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerPageBlogPost extends Controller
{
    private $error = '';

    /**
     * Главный метод инициализации компонента
     *
     * @return void
     */
    public function index()
    {
        $this->storage->post = [];

        if ($this->hasValidation()) {
            $this->storage->post = $this->model->getPost($this->args->page);
        }

        if (!$this->storage->post) {
            $this->view->initComponent('site.Pages.Page404@index');
            return;
        }

        $breadcrumbs = [
            [ 'name' => $this->language->get('Блог автора | разработчика'), 'link' => '/blog/posts' ],
            [ 'name' => $this->storage->post['title'], 'link' => '' ]
        ];
        $breadcrumbs = $this->storage->header = $this->view->loadComponent('site.Common.Breadcrumbs@index', ['breadcrumbs' => $breadcrumbs ]);

        $header = [
            'title'         => $this->storage->post['title'],
            'breadcrumbs'   => $this->language->get($breadcrumbs),
            'meta_title'    => $this->storage->post['meta_title'],
            'meta_desc'     => $this->storage->post['meta_description'],
        ];
        $this->storage->header = $this->view->loadComponent('site.Common.Header@index', $header);

        $this->storage->footer = $this->view->loadComponent('site.Common.Footer@index');

        $this->view->setHttpBody($this->storage);
    }

    /**
     * Метод, который проверяет параметры на валидацию
     *
     * @return boolean
     */
    private function hasValidation() : bool
    {
        if (!$this->args->page || !UtiliteValidations::isUrlPathRequest($this->args->page)) {
            $this->error = $this->language->get('<nobr>Страница не найдена</nobr> | <nobr>Not Found</nobr>');
        }

        return !$this->error;
    }
}