<?php

namespace components\site\Common\controllers;

use system\component\Controller;


/**
 * ControllerHeader class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerHeader extends Controller
{
	/**
     * Главный метод инициализации компонента
     *
     * @return void
     */
    public function index()
    {
		$this->storage->css = [
			[ 'url' => URL_ASSETS . 'css/site/main.min.css', 'version' => '1.0.0' ]
		];

		$this->storage->title = DOMAIN_NAME;
		if ($this->args->title) {
			$this->storage->title = $this->args->title;
		}

		$this->storage->canonical = DOMAIN_NAME . substr($this->request->url, 0, -1);

		$this->storage->desc = $this->args->desc;
		$this->storage->breadcrumbs = $this->args->breadcrumbs;

		$this->storage->meta_desc = $this->args->meta_desc;
		$this->storage->meta_title = ($this->args->meta_title) ? $this->args->meta_title : $this->args->title;

		$this->storage->nav = [];
		if ($this->request->url !== '/') {
			$this->storage->nav[] = [ 'name' => $this->language->get('Главная'), 'link' => '/', 'type' => 'link' ];
		}

		$this->storage->nav[] = [ 'name' => $this->language->get('Об авторе'), 'link' => '/#about', 'type' => '#' ];
		$this->storage->nav[] = [ 'name' => $this->language->get('Контакты'), 'link' => '#contact', 'type' => '#' ];
		$this->storage->nav[] = [ 'name' => $this->language->get('Блог'), 'link' => '/blog/posts', 'type' => 'link' ];

    	$this->view->setHttpBody($this->storage);
    }
}