<?php

namespace components\site\Common\controllers;

use system\component\Controller;


/**
 * ControllerPagination class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerPagination extends Controller
{
    /**
     * Максимальное кол-во элементов, в каждое направление от текущего
     */
    private const MAX_ITEMS_EDGES = 2;

    /**
     * Содержит ошибки обработки
     *
     * @var array
     */
    private $errors = [];

    /**
     * Главный метод инициализации компонента
     * 
     * $this->args->page; - текущая страница
     * $this->args->total; - кол-во элементов
     * $this->args->limit; - кол-во элементов на страницу
     * $this->args->link; - ссылка страницы
     * $this->args->key; - ключ пагинации
     *
     * @return void
     */
    public function index()
    {
        if ($this->hasValidation()) {
            $pages = ceil($this->args->total / $this->args->limit);
            if ($pages == 1) {
                return;
            }

            if (!$this->args->key) {
                $this->args->key = 'page';
            }

            $data = [];
            for ($i = 1; $i <= $pages; $i++) {

                $link = $this->args->link;
                if ($i !== 1 ) {
                    $link .= '?' . $this->args->key . '=' . $i;
                }

                $data[$i] = [
                    'link'      => $link,
                    'active'    => ($i == $this->args->page) ? true : false,
                    'type'      => 'link',
                    'name'      => $i,
                ];
            }

            $this->storage->list[$this->args->page] = $data[$this->args->page];
            
            $indexFirst = 0;
            $indexLast = 0;
            for ($i = 1; $i <= self::MAX_ITEMS_EDGES; $i++) {
                $indexFirst = $this->args->page + ($i * -1);
                $indexLast = $this->args->page + $i;

                if ($data[$indexLast]) {
                    $this->storage->list[$indexLast] = $data[$indexLast];
                }

                if ($data[$indexFirst]) {
                    $this->storage->list[$indexFirst] = $data[$indexFirst];
                }
            }

            asort($this->storage->list);

            $this->storage->first = $data[1];
            if (!isset($this->storage->list[1])) {
                $this->storage->list[1] = [
                    'type'      => '',
                    'name'      => '...',
                ];

                asort($this->storage->list);
            }

            $this->storage->last = $data[count($data)];
            if (!isset($this->storage->list[count($data)])) {
                $this->storage->list[count($data)] = [
                    'type'      => '',
                    'name'      => '...',
                ];

                asort($this->storage->list, SORT_NATURAL);
            }

        } else {
            $this->storage = print_r($this->errors, true);
        }

        $this->view->setHttpBody($this->storage, true);
    }

    /**
     * Метод, который проверяет параметры на валидацию
     *
     * @return boolean
     */
    private function hasValidation() : bool
    {
        if (!$this->args->page || !$this->args->total || !$this->args->limit || !$this->args->link) {
            $this->errors['error'] = 'Не переданы параметры с ключами: page, total, limit, link';
        }

        return !$this->errors;
    }
}