<?php

namespace components\site\Common\controllers;

use system\component\Controller;


/**
 * ControllerBreadcrumbs class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerBreadcrumbs extends Controller
{
    /**
     * Главный метод инициализации компонента
     *
     * @return void
     */
    public function index()
    {
		$this->storage->list = [
            [ 'name' => $this->language->get('Главная'), 'link' => '/' ]
        ];

        $this->storage->count = 0;

        if ($this->args->breadcrumbs) {
            foreach ($this->args->breadcrumbs as $breadcrumbs) {
                $this->storage->list[] = $breadcrumbs;
            }

            $this->storage->count = count($this->args->breadcrumbs);
        }

    	$this->view->setHttpBody($this->storage);
    }
}