

	</div>
	
	<footer class="footer">

		<section class="footer-contact">
			<h2 class="footer-contact__title animated" id="contact"><span class="color-blue">#</span> <?php $language->the('Контакты') ?></h2>

			<p>г. Санкт-Петербург</p>

			<div class="footer-contact__info">
				<span><a href="mailto:rodion-krox@mail.ru" rel="nofollow">rodion-krox@mail.ru</a></span>
				<span class="color-blue"> | </span> 
				<span>skype: rodionads1</span>
				<br>
				<a href="https://vk.com/manushov_rodion" target="_blank" rel="nofollow">Вконтакте</a>
				<span class="color-blue"> | </span>
				<a href="https://moikrug.ru/manushovrodion" target="_blank" rel="nofollow">Мой круг</a>
			</div>

		</section>

	</footer>
	
	<?php if (isset($storage->js)) : foreach ($storage->js as $js) : ?>
		<script src="<?php echo $js['url'] ?>?version=<?php echo $js['version'] ?>"></script>
	<?php endforeach; endif ?>

	<!-- Yandex.Metrika counter --> 
	<!-- <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter50262118 = new Ya.Metrika2({ id:50262118, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/tag.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks2"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/50262118" style="position:absolute; left:-9999px;" alt="" /></div></noscript> -->
	<!-- /Yandex.Metrika counter -->
	
	</body>
</html>