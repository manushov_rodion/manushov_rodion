<nav class="breadcrumbs">
	<?php foreach ($storage->list as $breadcrumbs) : ?>
		<?php if($breadcrumbs['link'] !== '' && $storage->count > 0) : ?>
			<a class="breadcrumbs__item" href="<?php echo $breadcrumbs['link'] ?>"><?php echo $breadcrumbs['name'] ?></a> 
			<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 17 17" style="fill: rgb(222, 222, 222);"><path d="M9.644 8.5L2.79 15.354l-.707-.707L8.229 8.5 2.083 2.354l.707-.708L9.644 8.5zm-2.01-6.854l-.707.708L13.073 8.5l-6.146 6.146.707.707L14.487 8.5 7.634 1.646z"/></svg> 
		<?php else: ?>
			<span class="breadcrumbs__item breadcrumbs__item_active"><?php echo $breadcrumbs['name'] ?></span>
		<?php endif ?>
	<?php endforeach ?>
</nav>