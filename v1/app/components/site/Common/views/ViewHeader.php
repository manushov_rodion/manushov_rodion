<!DOCTYPE html>
<html lang="<?php echo $language->lang ?>">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">

		<meta name="description" content="<?php echo $storage->meta_desc ?>" />

		<link rel="apple-touch-icon" sizes="180x180" href="/v1/app/assets/img/favicons/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/v1/app/assets/img/favicons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/v1/app/assets/img/favicons/favicon-16x16.png">
		<link rel="manifest" href="/v1/app/assets/img/favicons/site.webmanifest">
		<meta name="msapplication-TileColor" content="#242d37">
		<meta name="theme-color" content="#242d37">

		<link rel="canonical" href="<?php echo $storage->canonical ?>" />

	<?php if (isset($storage->css)) : foreach ($storage->css as $css) : ?>
		<link rel="stylesheet" href="<?php echo $css['url'] ?>?version=<?php echo $css['version'] ?>">
	<?php endforeach; endif ?>

		<title><?php echo $storage->meta_title ?></title>
	</head>
	<body>

		<section class="top-bar">
			<div class="container">
				<div class="grid">

					<div class="grid__col grid__col_sm-50">
						<h3 class="header-logo">manushov_rodion</h3>
					</div>

					<div class="grid__col grid__col_sm-50">
						<nav class="navigation-horizontal">
							<?php foreach ($storage->nav as $nav) : ?>
								<a class="navigation-horizontal__item" href="<?php echo $nav['link'] ?>">
									<?php if($nav['type'] == 'link') : ?>
										<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 17 17">
											<path d="M12.983 6.94l-.938.938-.707-.707.938-.938c.975-.975.975-2.561 0-3.535s-2.561-.975-3.535 0L5.754 5.686c-.975.975-.975 2.561 0 3.535s2.561.975 3.535 0l.707.707c-.683.683-1.578 1.023-2.475 1.023s-1.792-.341-2.474-1.023a3.504 3.504 0 0 1 0-4.949l2.987-2.987a3.502 3.502 0 0 1 4.949 0 3.501 3.501 0 0 1 0 4.948zM6.042 8.034l-.13.129.705.709.131-.13c.975-.975 2.561-.975 3.535 0s.975 2.561 0 3.535L7.26 15.302c-.975.975-2.561.975-3.535 0s-.975-2.561 0-3.535l1.058-1.059-.707-.707-1.058 1.059a3.504 3.504 0 0 0 0 4.949c.683.683 1.578 1.023 2.475 1.023s1.792-.341 2.475-1.023l3.023-3.024a3.504 3.504 0 0 0 0-4.949 3.503 3.503 0 0 0-4.949-.002z"/>
										</svg>
									<?php else: ?>
										<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 17 17"><path d="M3 3v11h11V3H3zm10 10H4V4h9v9z"/></svg>
									<?php endif ?> 
									<span><?php echo $nav['name'] ?></span>
								</a>
							<?php endforeach ?>
						</nav>
					</div>

				</div>
			</div>
		</section>

		<header class="header">
			<div class="container">
				<h1 class="header__title"><?php echo $storage->title ?></h1>
				<?php if ($storage->desc) : ?>
					<p class="header__desc"># <?php echo $storage->desc ?></p>
				<?php endif ?>

				<?php if ($storage->breadcrumbs) : ?>
					<?php echo $storage->breadcrumbs ?>
				<?php endif ?>
			</div>
		</header>

		<div class="wrapper">