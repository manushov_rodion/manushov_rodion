<?php

namespace components\site\XML\models;

use system\component\Model;


/**
 * ModelFooter class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ModelSitemap extends Model
{

    /**
     * Форматирует дату в формат для карты сайта.
     *
     * @param string $date
     * @return string
     */
    private function formatData(string $date) : string
    {
        $date = new \DateTime($date);
        return $date->format('Y-m-d\TH:iP');
    }

    /**
     * Метод, который возвращает данные по статьям
     *
     * @return array
     */
    public function getPosts() : array
    {
        $data = [];

        $query = $this->db->query('SELECT link, date_lastmod FROM ' . DB_PREFIX . 'blog__posts WHERE status = true AND lang = ? ORDER BY date_lastmod DESC', ['ru']);

        if ($query->rows) {
            foreach ($query->rows as $row) {
                $data[] = [
                    'loc'       => DOMAIN_NAME . '/blog/posts/' . $row->link,
                    'lastmod'   => $this->formatData($row->date_lastmod),
                ];
            }
        }

        return $data;
    }
}