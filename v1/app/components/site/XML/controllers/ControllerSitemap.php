<?php

namespace components\site\XML\controllers;

use system\component\Controller;


/**
 * ControllerFooter class
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ControllerSitemap extends Controller
{
    public function index()
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
        
        $xml .= $this->getXmlUrl(DOMAIN_NAME, '1.0');

        // Записи блога
        $posts = $this->model->getPosts();
        if ($posts) {
            foreach ($posts as $post) {
                $xml .= $this->getXmlUrl($post['loc'], '0.9', $post['lastmod']);
            }
        }

        $xml .= $this->getXmlUrl(DOMAIN_NAME . '/blog/posts');
        
        $xml .= '</urlset>';

        $this->view->setHttpHeader('Content-Type', 'application/xml');
    	$this->view->setHttpBody($xml, true);
    }

    /**
     * Метод, который формирует тег url для xml
     *
     * @param string $loc
     * @param string $priority (0.5)
     * @param string $lastmod (2018-09-05T15:02+07:00)
     * @return string
     */
    private function getXmlUrl(string $loc, string $priority = '0.5', string $lastmod = '') : string
    {
        if (!$lastmod) {
            $lastmod = date('Y-m-d\TH:iP');
        }

        $xml = '<url>';
            $xml .= '<loc>' . $loc . '/</loc>';
            $xml .= '<lastmod>' . $lastmod . '</lastmod>';
            $xml .= '<priority>' . $priority . '</priority>';
        $xml .= '</url>';

        return $xml;
    }
}