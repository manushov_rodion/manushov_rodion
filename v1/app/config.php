<?php
$_['debug'] = true;

// Подключение к БД
$_['db_connect'] = [
    'active'        => true,
    'host_name'     => 'localhost',
    'user_name'     => 'postgres',
    'password'      => '',
    'name'          => 'manushov_rodion',
    'port'          => 5432,
    'prefix'        => 'xyz_',
    'driver'        => 'pgsql',
];

// Компонент для 404 страницы
$_['data_route_404'] = [
    'component'         => 'site.Pages.Page404@index',
    'args'              => [],
];

// Настройки авторизации
$_['auth'] = [
    'active'        => true,
    'type'          => 'session',  // http
    'url_redirect'  => '/xyz/auth',
    'expire'        => 60 * 60 * 3, // секунды * минуты * часы * дни * месяцы * годы
    'cookie_path'   => '/xyz',
];

// Заголовки по умолчанию для ответа на запрос
$_['http_headers'] = [
    'HTTP/1.1'                      => '200 OK',
    'Accept-Charset'                => 'utf-8',
    'Content-Language'              => 'ru',
    'Access-Control-Allow-Methods'  => 'GET, POST',
    'Access-Control-Allow-Headers'  => 'Content-Type',
    'Content-Type'                  => 'text/html', // application/json
];

// Настройка view
$_['view'] = [
    'type_file'       => 'text/html', // application/json
    'cache_status'    => false,
];

// Настройка languages
$_['languages'] = [
    'default_lang' => 'ru'
];

// Перечень IP и AGENT, которые необходимо заблокировать
$_['blocking'] = [
    'ips'       => [],
    'agents'    => [],
];
