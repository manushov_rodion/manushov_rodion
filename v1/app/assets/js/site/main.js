/**
 * Скролл
 */
function scroll_by_hook($link, $event) {
    var hash = '';
    var sections = document.querySelectorAll('.animated');
    sections.forEach(function(section) {
        section.classList.remove('pulse');
    });

    if (!$link || !(hash = $link.split('#')[1]) ) {
        return;
    }

    var section = document.getElementById(hash);
    if (!section) {
        return;
    }

    if ($event) {
        $event.preventDefault();
    }

    window.scrollTo({
        top: section.getBoundingClientRect().top - 50,
        behavior: 'smooth'
    });

    section.classList.add('pulse');
}

document.querySelector('.navigation-horizontal').addEventListener('click', function(event) {
    var link = event.target.parentElement.href;
    scroll_by_hook(link, event)
});

if (window.location.hash) {
    var link = location.href;
    window.location.hash = '';
    scroll_by_hook(link, null);
}