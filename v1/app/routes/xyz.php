<?php

/**
 * Файл списка доступных маршрутов.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 * 
 * @param string $url_request
 * @param string $component
 * @param bool $access_public
 * @return void
 * 
 * $route->get(...);
 * $route->post(...);
 * $route->put(...);
 * $route->delete(...);
 */

$route->get('/xyz', 'admin.Pages.Desktop@index');

$route->get('/xyz/auth', 'admin.Pages.PageAuth@index', true);
$route->get('/xyz/auth/exit', 'admin.Pages.PageAuth@exit');
$route->post('/xyz/auth', 'admin.Pages.PageAuth@index', true);