<?php

/**
 * Файл списка доступных маршрутов.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 * 
 * @param string $url_request
 * @param string $component
 * @param bool $access_public
 * @return void
 * 
 * $route->get(...);
 * $route->post(...);
 * $route->put(...);
 * $route->delete(...);
 */

$route->get('/', 'site.Pages.FrontPage@index', true);
$route->get('/blog/posts', 'site.Pages.PageBlog@index', true);
$route->get('/blog/posts/{page}', 'site.Pages.PageBlogPost@index', true);

$route->get('/sitemap.xml', 'site.XML.Sitemap@index', true);