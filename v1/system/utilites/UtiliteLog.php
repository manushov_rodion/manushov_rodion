<?php

namespace system\utilites;

/**
 * UtiliteLog class
 * - Класс который, отвечает за логирование данных
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class UtiliteLog
{
    /**
     * Сохраняет данные в лог файл, с текущей меткой времени.
     *
     * @param string $file_name
     * @param string $message
     * @return void
     */
    final public static function set(string $file_name, string $message) : void
    {
        $file = DIR_STORAGE . 'logs/' . $file_name . '.php';
        
        if (!file_exists($file)) {
            $chunks = explode('/', $file_name . '.php');
            $path = DIR_STORAGE . 'logs/';

            foreach ($chunks as $value) {
                if (strpos($value, '.php') === false) {
                    $path .= $value . '/';
                }
            }

            if (!file_exists($path)) {
                if (!mkdir($path, 0777, true)) {
                    die('Не удалось создать папку для логов');
                }
            }

            file_put_contents($file, "<?php\r\n", FILE_APPEND);
        }

        $date = date('Y-m-d H:i:s');
        $text = "\r\n\$_['" . $date . " | " . $message . "'];";

        file_put_contents($file, $text, FILE_APPEND);
    }

    /**
     * Метод, который фиксирует подключение пользователя к сайту.
     *
     * @param string $note
     * @return void
     */
    final public static function setInfoUserConnect(string $note = null) : void
    {
        $message = $_SERVER['REMOTE_ADDR'] . ' | ' . $_SERVER['HTTP_USER_AGENT'];

        if ($note) {
            $message = $note . ': ' . $message;
        }

        self::set('user_connect', $message);
    }
}