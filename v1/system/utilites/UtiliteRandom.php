<?php

namespace system\utilites;

/**
 * UtiliteRandom class
 * - Класс который генерит случайные значения
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class UtiliteRandom
{
    /**
     * Метод, который возвращает рандомный секретный ключ.
     *
     * @param integer $length
     * @return string
     */
    public static function randomSecretKey(int $length = 32) : string
    {
        $string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789}{@#|$?*~%';
        $max = strlen($string) - 1;
        
        $token = '';
        for ($i = 0; $i < $length; $i++) {
            $token .= $string[mt_rand(0, $max)];
        }	
        
        return $token;
    }

    /**
     * Генератор Хеша, на базе hash_hmac и MD5
     *
     * @param string $data
     * @param string $secret_key
     * @param string $alg = 'sha256'
     * @return string
     */
    public static function getHash(string $data, string $secret_key, string $alg = 'sha256') : string
    {
        $md5 = md5($data);
        $l = ceil(strlen($md5) / 2);
        $hash = substr($md5, 0, $l) . hash_hmac($alg, $data, $secret_key) . substr($md5, $l, strlen($md5));
        
        return $hash;
    }
}