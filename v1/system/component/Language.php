<?php

namespace system\component;

/**
 * Language class
 * - Класс, который содержит в себе локализацию.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class Language
{
    /**
     * Содержит всю доступную локализацию
     *
     * @var array
     */
    private static $data = [];

    /**
     * Содержит текущую локализацию
     *
     * @var string
     */
    public $lang = '';

    /**
     * Метод, который возвращает значение по ключу
     *
     * @param string $index
     * @return string
     */
    final public function get(string $index) : string
    {
        if (isset(self::$data[$index])) {
            return self::$data[$index];
        }

        return $index;
    }

    /**
     * Метод, который возвращает все ключи и их значения локализации
     *
     * @return array
     */
    final public function getAll() : array
    {
        return self::$data;
    }
  
    /**
     * Метод, который выводит значение по ключу.
     *
     * @param string $index
     * @return void
     */
    final public function the(string $index) : void
    {
      echo $this->get($index);
    }

    /**
     * Метод, который загружает данные файла, в  хранилище локализации
     *
     * @param string $file
     * @return void
     */
    final public function load(string $file) : void
    {
        $_ = [];
        if (file_exists($file)) {
            include $file;
        }

        if ($_) {
            foreach ($_ as $key => $value) {
                self::$data[$key] = $value;
            }
        }
    }
}