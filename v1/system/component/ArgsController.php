<?php

namespace system\component;

use system\library\Traits\TraitGeterSeter;

/**
 * ArgsController class
 * - Формирует правильный вид для агументов, переданных в контроллер.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ArgsController
{
    use TraitGeterSeter;

    final public function __construct(array $args)
    {
        if ($args) {
            foreach ($args as $key => $value) {
                if($value) {
                    $this->$key = $value;
                }
            } 
        }
    }
}