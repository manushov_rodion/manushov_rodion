<?php

namespace system\component;

use system\library\Cache\CacheDiagram;
use system\library\HTTP\ParamsRequest;

/**
 * Component class
 * - Класс, который инициализирует компонент.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class Component
{
    /**
     * Содержит название компонента.
     *
     * @var string
     */
    private $name = '';

    /**
     * Содержит название экшен компонента
     * 
     *
     * @var string
     */
    private $action = '';

    /**
     * Пространство, где расположен компонент
     *
     * @var string
     */
    private $namespace = 'components\\';

    /**
     * Содержин экземпляр Config
     *
     * @var Config
     */
    public $config = null;

    /**
     * Аргументы компонента
     *
     * @var array
     */
    public $args_component = [];

    /**
     * Содержит адаптер авторизации
     *
     * @var system\library\Auth\AdapterAuth
     */
    public $auth_adapter = null;

    /**
     * Принимает контроллер и определяет его свойства.
     *
     * @param string $component
     */
    final public function __construct(string $component)
    {
        [$component, $this->action] = explode('@', $component);

        if (strpos($component, '.') === false) {
            $this->name = $component;
        } else {
            $chunks = explode('.', $component);
            $this->name = array_pop($chunks);
            $this->namespace .= implode('\\', $chunks) . '\\';
        }
    }

    /**
     * Возвращает имя компонента
     *
     * @return string
     */
    final public function getComponentName() : string
    {
        return $this->name;
    }

    /**
     * Главная функция, которая занимается обработкой данных и инициализайией компонента
     *
     * @return array
     */
    final public function main() : array
    {
        $controller = $this->namespace . 'controllers\\Controller' . $this->name;
        $controller = new $controller();

        $controller->args = new ArgsController($this->args_component);
        $controller->params = new ParamsRequest();

        $language = new Language();
        $language->lang = $this->config->getOption('languages.lang');
        $language->load(DIR_APP . 'languages/' . $this->config->getOption('languages.lang') . '/index.php');
        $language->load(DIR_APP . str_replace('\\', '/', $this->namespace) . 'languages/' . $this->config->getOption('languages.lang') . '/Language' . $this->name . '.php');

        $controller->language = $language;

        $controller->auth = new Auth($this->auth_adapter);

        $model = $this->namespace . 'models\\Model' . $this->name;
        if(class_exists($model)) {
            $controller->model = new $model();
        }

        $controller->view = new View($this->config, DIR_APP . str_replace('\\', '/', $this->namespace) . 'views/View', $this->name);
        $controller->view->language = $language;
        $controller->view->auth_adapter = $this->auth_adapter;

        $controllerMethod = $this->action;
        $controller->$controllerMethod();

        $result = $controller->view->getResult();
        return [
            $result['http_headers'],
            $result['http_body']
        ];
    }
}
