<?php

namespace system\component;

use system\library\Auth\Adapters\Abstract__AuthAdapter;
use system\library\Auth\ModifiedJWT\EncodeToken;


/**
 * Auth class
 * - Класс, авторизации.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */

class Auth
{
    /**
     * Содержит адаптер авторизаии
     *
     * @var system\library\Auth\Adapters\Abstract__AuthAdapter|null
     */
    private $adapter = null;

    /**
     * Содержит токен авторизации
     *
     * @var string
     */
    public $token = '';

    /**
     * Содержит статус авторизации
     *
     * @var boolean
     */
    public $status = false;

    /**
     * Адрес авторизации
     *
     * @var string
     */
    public $url = '';

    /**
     * ID Пользователя
     *
     * @var string
     */
    public $user_id = '';

    /**
     * ID сессии
     *
     * @var string
     */
    public $session_id = '';

    /**
     * @param Abstract__AuthAdapter|null $adapter
     */
    final public function __construct(?Abstract__AuthAdapter $adapter)
    {
        if ($adapter) {
            $this->adapter = $adapter;
            $this->token = $this->adapter->token;
            $this->status = $this->adapter->status;
            $this->url = $this->adapter->getOptions()['url_redirect'];
            if ($this->adapter->payload) {
                $this->session_id = $this->adapter->payload->id;
                $this->user_id = $this->adapter->payload->user_id;
            }
        }
    }

    /**
     * Метод, который фиксирует авторизацию
     *
     * @param string $user_id
     * @return void
     */
    final public function set(string $user_id)
    {
        if ($this->adapter) {
            $this->adapter->setAuth($user_id);
            $this->token = $this->adapter->token;
        }
    }

    /**
     * Метод, который удаляет авторизацию
     *
     * @param string $user_id
     * @return void
     */
    final public function delete(string $user_id)
    {
        if ($this->adapter) {
            $this->adapter->deleteAuth($user_id);
            $this->token = '';
            $this->status = false;
        }
    }
}