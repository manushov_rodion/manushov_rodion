<?php

namespace system\library\Traits;

/**
 * TraitGeterSeter trait
 * - trait, который содержит в себе гетеры и сетеоры.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
trait TraitGeterSeter
{
    /**
     * Содержит переменные сетеры и гетеры
     *
     * @var array
     */
    private $__vars = [];

    /**
     * Сетер
     *
     * @param string $name
     * @param any $value
     * @return void
     */
    final public function __set(string $name, $value) : void
    {
        $this->__vars[$name] = $value;
    }

    /**
     * Гетер
     *
     * @param string $name
     * @return any
     */
    final public function __get(string $name)
    {
        if (isset($this->__vars[$name])) {
            return $this->__vars[$name];
        }

        return null;
    }
}