<?php

namespace system\library\DataBase;

use system\library\Config;

/**
 * PDOConnect class
 * - Класс, который инициализирует подключение к БД.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class PDOConnect
{
    /**
     * Содержит подключение к БД
     *
     * @var PDO
     */
    public static $connect = null;

    /**
     * Содержит драйвер подключения
     *
     * @var string
     */
    public static $driver = '';

    /**
     * @param Config $config
     */
    final public function __construct(Config $config)
    {
        $host = $config->getOption('db_connect.host_name');
        $driver = $config->getOption('db_connect.driver');
        $port = $config->getOption('db_connect.port');
        $name = $config->getOption('db_connect.name');
        $user = $config->getOption('db_connect.user_name');
        $password = $config->getOption('db_connect.password');

        try {
            self::$connect = new \PDO (
                $driver .':host='. $host .';port='. $port .';dbname=' . $name,
                $user, 
                $password,
                [\PDO::ATTR_PERSISTENT => true]
            );

            self::$driver = $driver;

            if (!defined('DB_PREFIX')) {
                define('DB_PREFIX', $config->getOption('db_connect.prefix'));
            }
        }
        catch (\PDOException $Exception) {
            trigger_error('Не удалось подключиться к бд! ' . $Exception->getMessage(), E_USER_ERROR);
        }
    }

    /**
     * Метод, который закрывает соединение.
     *
     * @return void
     */
    final public static function close() : void
    {
        self::$connect = null;
    }
}
