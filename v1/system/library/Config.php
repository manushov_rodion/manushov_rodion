<?php

namespace system\library;

/**
 * Config class
 * - Класс, который работает с настройками приложения.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class Config 
{
    /**
     * Содержит одномерный массив параметров настроек
     *
     * @var array
     */
    private static $data = [];

    /**
     * Метод, который фиксирует настройку, в локальной базе конфига.
     *
     * @param string $key
     * @param string $value
     * @return void
     */
    final public function setOption(string $key, string $value) : void
    {
        $this->saveOption(self::$data, $key, $value);
    }

    /**
     * Метод, который сохраняет настройку, в зависимости от архитектуры ключа настройки.
     *
     * @param array $data
     * @param string $key
     * @param any $value
     * @return void
     */
    private function saveOption(array &$data, string $key, $value)
    {
        $keys = explode('.', $key);
        $firstKey = array_shift($keys);

        if ($keys) {
            $key = implode('.', $keys);

            if (!isset($data[$firstKey])) {
                $data[$firstKey] = [];
            }

            $this->saveOption($data[$firstKey], $key, $value);
        }
        else {
            if (isset($data[$firstKey]) && is_array($data[$firstKey])) {
                exit('Ключ значения "' . $firstKey . '" опции имеет продолжение ключей! - нельзя переопределить.');
            }

            $data[$firstKey] = $value;
        }
    }

    /**
     * Метод, который возвращает значение настройки.
     *
     * @param string $key
     * @return any
     */
    final public function getOption(string $key) 
    {
        return $this->searchOption($key, self::$data);
    }

    /**
     * Метод, который ищет значение настройки.
     *
     * @param string $key
     * @param array $data
     * @return any
     */
    private function searchOption(string $key, array $data)
    {
        $keys = explode('.', $key);
        $firstKey = array_shift($keys);

        if (!isset($data[$firstKey])) {
            return null;
        }

        if ($keys) {
            $key = implode('.', $keys);
            return $this->searchOption($key, $data[$firstKey]);
        }
        else {
            return $data[$firstKey];
        }
    }

    /**
     * Возвращает все доступные настройки.
     *
     * @return array
     */
    final public function getOptions() : array
    {
        return self::$data;
    }

    /**
     * Метод, который подключает файл конфигурации и берет с него настройки приложения
     *
     * @param string $filename
     * @return boolean
     */
    final public function loadOptionsFromFile(string $filename) : bool
    {
        $_ = [];

        if (file_exists($filename)) {
            include $filename;
        } 
        else {
            return false;
        }

        if ($_ && is_array($_)) {
            foreach ($_ as $key => $option) {
                if (!isset(self::$data[$key]) || !is_array($option)) {
                    self::$data[$key] = $option;
                } 
                else {
                    foreach ($option as $key_option => $value) {
                        if (!self::$data[$key]) {
                            self::$data[$key] = [];
                        }

                        self::$data[$key][$key_option] = $value;
                    }
                }
            }
        }

        return true;
    }
}