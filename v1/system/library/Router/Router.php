<?php

namespace system\library\Router;

use system\library\Config;


/**
 * Router class
 * - Класс, который работает с запросом и ищет текущий запрос, в базе доступных маршрутов.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class Router
{
    /**
     * Содержит адрес запроса.
     *
     * @var string
     */
    private $url_request = '';

    /**
     * Содержит массив доступных маршрутов.
     *
     * @var array
     */
    private $routes = [];

    /**
     * Содержит данные по маршруту.
     *
     * @var array
     */
    private $data_route = [];

    final public function __construct()
    {
        $this->url_request = parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH );

        $route = new Route;
        (function($route) 
        {
            foreach (glob(DIR_APP . 'routes/*.php') as $file_route) {
                include $file_route;
            } 
        })($route);

        $this->routes = $route->getRoutes($_SERVER['REQUEST_METHOD']);
    }

    /**
     * Метод, который проверяет текущий запрос в списке доступных маршрутов.
     *
     * @return boolean
     */
    final public function hasRoute() : bool
    {
        $status = false;

        if ($this->routes) {
            foreach ($this->routes as $regular => $data) {
                if (preg_match($regular, $this->url_request, $resRregular)) {
                    array_shift($resRregular);
                    
                    $this->setDataRoute($data, $resRregular);
                    $status = true;
                }
            }
        }

        return $status;
    }

    /**
     * Метод, который устанавливает данные маршрута.
     *
     * @param array $data
     * @param array $data_args
     * @return void
     */
    private function setDataRoute(array $data, array $data_args = []) : void
    {
        if ($data['args'] && isset($data_args)) {
            foreach ($data['args'] as $key => $value) {
                if(isset($data_args[$key])) {
                    $data['args'][$value] = $data_args[$key];
                } else {
                    $data['args'][$value] = '';
                }

                unset($data['args'][$key]);
            }
        }

        $data['request'] = $this->url_request;
        $this->data_route = $data;
    }

    /**
     * Метод, который возвращает данные по маршруту.
     *
     * @return array
     */
    final public function getDataRoute() : array
    {
        return $this->data_route;
    }

    /**
     * Метод, который возвращает данные по 404 маршруту.
     *
     * @param Config $config
     * @return array
     */
    final public function getDataRoute404(Config $config) : array
    {
        $component = $config->getOption('data_route_404.component');

        if (!$component) {
            trigger_error('Не определен компонент 404 страницы, в настройках!', E_USER_ERROR);
        }

        return [
            'component'     => $component,
            'args'          => $config->getOption('data_route_404.args'),
            'access_public' => true,
            'request'       => null
        ];
    }
}