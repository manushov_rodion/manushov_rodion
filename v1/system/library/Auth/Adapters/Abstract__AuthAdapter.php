<?php

namespace system\library\Auth\Adapters;

/**
 * Abstract__AuthAdapter class
 * - Базовый класс для адаптеров.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
abstract class Abstract__AuthAdapter
{
    /**
     * Содержит настройки
     *
     * @var array
     */
    protected $options = [];

    /**
     * Токен авторизации
     *
     * @var string
     */
    public $token = '';

    /**
     * Открытый контент
     * 
     * @var array
     */
    public $payload = [];

    /**
     * Содержит статус авторизации.
     *
     * @var boolean
     */
    public $status = false;

    /**
     * Поддержка базы данных.
     *
     * @var boolean
     */
    public $db_active = false;

    /**
     * Содержит заголовок HTTP ответа.
     *
     * @var []
     */
    public $http_headers = '';

    /**
     * Содержит тело ответа на запрос.
     *
     * @var string
     */
    public $http_body = '';

    /**
     * Инициализирует авторизацию
     *
     * @return void
     */
    abstract public function initAuth() : void;

    /**
     * Метод, который фиксирует авторизацию
     *
     * @return void
     */
    abstract public function setAuth(string $user_id) : void;

    /**
     * Метод, который удаляет авторизацию
     *
     * @param string $user_id
     * @return void
     */
    abstract public function deleteAuth(string $user_id) : void;

    /**
     * Регестрирует настройки авторизации
     *
     * @param array $options
     * @return void
     */
    final public function setOptions(array $options) : void
    {
        $this->options = $options;
    }

    /**
     * Возвращает перечень настроек с параметрами
     *
     * @return array
     */
    final public function getOptions() : array
    {
        return $this->options;
    }
}