<?php

namespace system\library\Auth\Adapters;

use system\library\Auth\ModifiedJWT\EncodeToken;
use system\library\DataBase\PDORequest;
use system\library\Auth\ModifiedJWT\DecodeToken;


/**
 * AdapterAuthSession class
 * - Адаптер авторизации, по сессии.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class AdapterAuthSession extends Abstract__AuthAdapter
{
    /**
     * Инициализирует авторизацию
     *
     * @return void
     */
    public function initAuth() : void
    {
        if (isset($_COOKIE['USER_SESSION']) && $_COOKIE['USER_SESSION']) {
            if ($this->db_active === true) {
                $jwt = new DecodeToken();
                $jwt->token = $_COOKIE['USER_SESSION'];
                $jwt->init();
                if ($jwt->payload) {
                    $this->payload = $jwt->payload;
                    $this->token = $jwt->token;
                    $this->status = true;
                }
            } else {
                trigger_error('Чтобы работала авторизация - необходимо активировать базу данных!', E_USER_ERROR);
            }
        }

        if ($this->status == false) {
            $redirect = '/';
            if ($this->options['url_redirect']) {
                $redirect = $this->options['url_redirect'];
            }

            $this->http_headers = [
                'Location' => DOMAIN_NAME . $this->options['url_redirect'],
                'HTTP/1.1' => '302 Found'
            ];
        }
    }

    /**
     * Метод, который фиксирует авторизацию
     *
     * @return void
     */
    public function setAuth(string $user_id) : void
    {
        if ($this->db_active === true) {
            $jtw = new EncodeToken();
            $jtw->user_id = $user_id;
            $jtw->exp = time() + $this->getOptions()['expire'];
            $jtw->init();
            $this->token = $jtw->token;

            setcookie('USER_SESSION', $this->token, $jtw->exp, $this->getOptions()['cookie_path'], $_SERVER['SERVER_NAME'], $_SERVER['HTTPS']);
        } else {
            trigger_error('Чтобы работала авторизация - необходимо активировать базу данных!', E_USER_ERROR);
        }
    }

    /**
     * Метод, который удаляет авторизацию
     *
     * @param string $user_id
     * @return void
     */
    public function deleteAuth(string $user_id) : void
    {
        $db = new PDORequest();
        $db->query('DELETE FROM xyz_session_auth WHERE exp < ?', [time()]);
        $db->query('DELETE FROM xyz_session_auth WHERE user_id = ?', [$user_id]);
        unset($_COOKIE['USER_SESSION']);
        setcookie('USER_SESSION', '', -1, $this->getOptions()['cookie_path'], $_SERVER['SERVER_NAME'], $_SERVER['HTTPS']);
    }
}