<?php

namespace system\library\Auth\ModifiedJWT;

use system\utilites\UtiliteRandom;



/**
 * Token class
 * - Базовый класс для работы с токеном авторизации
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
abstract class Token
{
    /**
     * Алгоритм шифрования
     *
     * @var string
     */
    protected $alg = 'SHA512';

    /**
     * Тип токена
     *
     * @var string
     */
    protected $typ = 'ModifiedJWT';

    /**
     * Декодирование строки из base64
     *
     * @param string $str
     * @return string
     */
    protected function base64Decode(string $str) : string
    {
        $remainder = strlen($str) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $str .= str_repeat('=', $padlen);
        }
        return base64_decode(strtr($str, '-_', '+/'));
    }

    /**
     * Кодирование строки в base64
     *
     * @param string $str
     * @return string
     */
    protected function base64Encode(string $str) : string
    {
        return str_replace('=', '', strtr(base64_encode($str), '+/', '-_'));
    }

    /**
     * Генератор Хеша на базе hash_hmac
     *
     * @param array $data_token
     * @param string $secret_key
     * @return string
     */
    protected function getHashHmac(array $data_token, string $secret_key) : string
    {
        $data_token = implode('_0_', $data_token);
        return UtiliteRandom::getHash($data_token, $secret_key, $this->alg);
    }

    /**
     * Метод, который возвращает рандомный секретный ключ.
     *
     * @param integer $length
     * @return string
     */
    protected function randomSecretKey(int $length = 32) : string 
    {
        return UtiliteRandom::randomSecretKey($length);
    }

    abstract public function init() : void;
}
