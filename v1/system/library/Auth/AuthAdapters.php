<?php

namespace system\library\Auth;

use system\library\Auth\Adapters\AdapterAuthSession;
use system\library\Auth\Adapters\AdapterAuthHttp;
use system\library\Auth\Adapters\Abstract__AuthAdapter;


/**
 * AuthAdaptersclass
 * - Класс, который определяет какой адаптер авторизации инициализировать.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */

class AuthAdapters
{
    /**
     * Типы авторизации
     *
     * @var array
     */
    private $types = [
        'session',
        'http'
    ];

    /**
     * Тип авторизации
     *
     * @var string
     */
    private $type = '';

    final public function __construct(string $type)
    {
        if (!in_array($type, $this->types)) {
            trigger_error('В настройках, выбран недопустимый вариант авторизации! Допустимые: "session" или "http"', E_USER_ERROR);
        }

        $this->type = $type;
    }

    /**
     * Метод, который возвращает адаптер авторизации
     *
     * @return AuthAdapter
     */
    final public function getAdapter() : Abstract__AuthAdapter
    {
        switch ($this->type) {
            case 'session':
                return new AdapterAuthSession();

            case 'http':
                return new AdapterAuthHttp();
        }
    }
}