<?php

namespace system\library\Cache;

/**
 * CacheRequest class
 * - Класс, который отвечает за работу с кешем запросов.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class CacheRequest
{
    /**
     * Расположение директории с кешем.
     *
     * @var string
     */
    public $directory = '';

    /**
     * Адрес запроса
     *
     * @var string
     */
    private $request = '';

    /**
     * Аргументы запроса.
     *
     * @var string
     */
    private $args_request = '';

    /**
     * Содержит наименование файла.
     *
     * @var string
     */
    private $file_name = '';

    final public function __construct()
    {
        $this->request = parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH );

        if ($this->request === '/') {
            $this->request = 'front_page';
        } else {
            $this->request = substr($this->request, 1);
            $this->request = str_replace('/', '_', $this->request);
        }

        if (isset($_GET) && $_GET) {
            $dataGet = [];
            foreach ($_GET as $key => $value) {
                $dataGet[] = $key . '#' . $value;
            }
            $this->args_request = implode('_', $dataGet);
        }
    }

    /**
     * Метод, в котором реализована работа с кешем
     *
     * @param string $file_name
     * @return void
     */
    public function main(string $file_name) : void
    {
        $this->directory .= $this->request . '/';

        $data_file_name = [];

        if ($this->args_request) {
            $data_file_name[] = $this->args_request;
        }

        $data_file_name[] = $file_name;

        $this->file_name = implode('__', $data_file_name) . '.php';
    }

    /**
     * Метод, который возвращает ответ: есть ли кеш?
     *
     * @return boolean
     */
    final public function hasCache() : bool
    {
        if (file_exists($this->directory . $this->file_name)) {
            return true;
        }
        return false;
    }

    /**
     * Метод, который возвращает кеш запроса
     *
     * @return array
     */
    final public function getCache() : array
    {
        $cache = '';

        if ($this->hasCache()) {
            ob_start(null, 0, PHP_OUTPUT_HANDLER_REMOVABLE);
            include $this->directory . $this->file_name;
            $cache = ob_get_contents();
            ob_end_clean();
        }

        $data = json_decode($cache, true);
        $data['http_body'] = html_entity_decode($data['http_body']);
        return $data;
    }

    /**
     * Удаляет кеш. В случае успеха возвращает true
     *
     * @return boolean
     */
    final public function removeCache() : bool
    {
        if ($this->hasCache()) {
            return unlink($this->directory . $this->file_name);
        }

        return true;
    }

    /**
     * Метод, который сохраняет контент запроса.
     *
     * @param string $http_body
     * @param array $http_headers
     * @return boolean
     */
    final public function saveCache(array $http_headers, string $http_body) : bool
    {
        if (!file_exists($this->directory)) {
            if(!mkdir($this->directory, 0777, true)) {
				die('Не удалось создать папку для кеша');
			}
        }
        
        if (!file_exists($this->directory) && !mkdir($this->directory, 0777, true)) {
            return false;
        }

        $data = [
            'http_headers'  => $http_headers,
            'http_body'     => htmlentities($http_body),
            'agent'         => $_SERVER['HTTP_USER_AGENT'],
            'user_ip'       => $_SERVER['REMOTE_ADDR'],
        ];

        if (file_put_contents($this->directory . $this->file_name, json_encode($data)) !== false) {
            return true;
        }

        return false;
    }
}