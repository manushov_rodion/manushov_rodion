<?php

namespace system\library\HTTP;

use system\library\Traits\TraitGeterSeter;


/**
 * HttpHeader class
 * - Класс, который содежит параметры переданные через POST, GET, PUT и DELETE.
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 * @version 1.0.0
 */
class ParamsRequest
{
    use TraitGeterSeter;

    final public function __construct()
    {
        $method = 'getDataRequest' . $_SERVER['REQUEST_METHOD'];
        $params = $this->$method();

        if ($params) {
            foreach ($params as $key => $value) {
                if ($value) {
                    $this->$key = $value;
                }
            }
        }
    }

    /**
     * Метод, который возвращает параметры GET запроса
     *
     * @return array
     */
    private function getDataRequestGET() : array
    {
        if (isset($_GET) && $_GET) {
            return $_GET;
        }

        return [];
    }

    /**
     * Метод, который возвращает параметры POST запроса
     *
     * @return array
     */
    private function getDataRequestPOST() 
    {
        if (isset($_POST) && $_POST) {
            return $_POST;
        }

        $data = json_decode(file_get_contents('php://input'));

        if (json_last_error() === 0) {
            return $data;
        }

        return [];
    }

    /**
     * Метод, который возвращает параметры PUT запроса
     *
     * @return array
     */
    private function getDataRequestPUT()
    {
        $data = json_decode(file_get_contents('php://input'));

        if (json_last_error() === 0) {
            return $data;
        }

        return [];
    }

    /**
     * Метод, который возвращает параметры DELETE запроса
     *
     * @return array
     */
    private function getDataRequestDELETE()
    {
        $data = json_decode(file_get_contents('php://input'));

        if (json_last_error() === 0) {
            return $data;
        }

        return [];
    }
}
