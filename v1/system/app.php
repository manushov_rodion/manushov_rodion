<?php

use system\library\Config;
use system\library\Router\Router;
use system\library\Cache\CacheRequest;
use system\library\HTTP\Response;
use system\component\Component;
use system\library\Auth\AuthAdapters;
use system\library\HTTP\HttpHeader;
use system\library\DataBase\PDOConnect;
use system\utilites\UtiliteLog;

/**
 * Файл инициализации проекта
 * 
 * @author Manushov Rodion <rodion-krox@mail.ru>
 */

$config = new Config();
$httpHeaders = new HttpHeader();
$response = new Response();

// Подгружаем настройки
$config->loadOptionsFromFile(DIR_SYSTEM . 'config/default.php');
$config->loadOptionsFromFile(DIR_APP . 'config.php');

// Блокировка пользователей
if ($config->getOption('blocking.ips')) {
    foreach ($config->getOption('blocking.ips') as $ip) {
        if ($ip == $_SERVER['REMOTE_ADDR']) {
            UtiliteLog::setInfoUserConnect('Заблокированный пользователь');
            $response->setHeader('HTTP/1.1', '403 Forbidden');
            $response->output();
        }
    }
}

if ($config->getOption('blocking.agents')) {
    foreach ($config->getOption('blocking.agents') as $agent) {
        if ($agent == $_SERVER['HTTP_USER_AGENT']) {
            UtiliteLog::setInfoUserConnect('Заблокированный пользователь');
            $response->setHeader('HTTP/1.1', '403 Forbidden');
            $response->output();
        }
    }
}

// Активация вывода всех ошибок
if ($config->getOption('debug') === true) {
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
}

// Определение языка и фиксирование его в настройках
$config->setOption('languages.lang', (function(Config $config) : string
{
    $lang = explode('.', $_SERVER['SERVER_NAME'])[0];
    if (strlen($lang) > 2) {
        $lang = $config->getOption('languages.default_lang');
    }

    return $lang;
})($config));

// Инициализация подключения к БД
if ($config->getOption('db_connect.active') === true) {
    new PDOConnect($config);
}

// Инициализация роутера
$router = new Router();
if ($router->hasRoute()) {
    $dataRoute = $router->getDataRoute();
} else {
    $dataRoute = $router->getDataRoute404($config);
}

// Инициализация адаптера авторизации
$authAdapter = null;
if ($config->getOption('auth.active') === true) {
    $authAdapter = new AuthAdapters($config->getOption('auth.type'));
    $authAdapter = $authAdapter->getAdapter();

    $authAdapter->db_active = $config->getOption('db_connect.active');
    $authAdapter->setOptions($config->getOption('auth'));

    if ($httpHeaders->hasAuthorization()) {
        $authAdapter->token = $httpHeaders->getValue('Authorization');
    }

    $authAdapter->initAuth();
}

// Подготавливаем ответ
foreach ($config->getOption('http_headers') as $key => $value) {
    if ($key === 'Content-Language') {
        $response->setHeader('Content-Language', $config->getOption('languages.lang'));
    } else {
        $response->setHeader($key, $value);
    }
}

// Инициализация обработки запроса
$http_headers = [];
$http_body = '';
if ($dataRoute['access_public'] == true || $authAdapter->status === true) {
    $cache = new CacheRequest;
    $component = new Component($dataRoute['component']);

    // инициализация настроек кеша
    $cache->directory = DIR_STORAGE . 'cache/template/' . $config->getOption('languages.lang') . '/';
    $cache->main($component->getComponentName());

    if ($config->getOption('view.cache_status') === true && $cache->hasCache()) {
        $result = $cache->getCache();
        $http_headers = $result['http_headers'];
        $http_body = $result['http_body'];
    } else {
        $component->config = $config;
        $component->args_component = $dataRoute['args'];
        $component->auth_adapter = $authAdapter;
        [$http_headers, $http_body] = $component->main();
    }
} else {
    if ($authAdapter->http_headers) {
        $http_headers = $authAdapter->http_headers;
    }

    if ($authAdapter->http_body) {
        $http_body = $authAdapter->http_body;
    }
}

if ($http_headers) {
    foreach ($http_headers as $key => $value) {
        $response->setHeader($key, $value);
    }
}

if ($http_body) {
    $response->setOutput($http_body);
}

$response->output();
